import { useSelector } from "react-redux";
import {
	increment,
	decrement,
	reset,
	incrementBy,
} from "./store/actions/actions";
import { useDispatch } from "react-redux";

function App() {
	const dispatch = useDispatch();
	const counter = useSelector((state) => state.counterReducer);
	return (
		<div className="App">
			<h1>Counter: {counter}</h1>
			<button onClick={() => dispatch(increment())}>+</button>
			<button onClick={() => dispatch(decrement())}>-</button>
			<button onClick={() => dispatch(reset())}>Reset</button>
			<button onClick={() => dispatch(incrementBy())}>+5</button>
		</div>
	);
}

export default App;
