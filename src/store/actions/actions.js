export const increment = () => {
	return {
		type: "INCREMENT",
	};
};

export const decrement = () => {
	return {
		type: "DECREMENT",
	};
};

export const reset = () => {
	return {
		type: "RESET",
	};
};

export const incrementBy = (value = 5) => {
	return {
		type: "INCREMENT_BY",
		payload: value,
	};
};
