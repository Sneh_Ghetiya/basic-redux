import { counterReducer } from "./reducers";
import { combineReducers } from "redux";

export const rootReducer = combineReducers({ counterReducer });
